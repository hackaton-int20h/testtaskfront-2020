# Test task int20h-2020
## [DEMO](https://d2hgkhkix7ynxs.cloudfront.net)
Make music recognition great again [requirements](docs/Test_task_web.pdf)

![screenshot](docs/demo.png)
## Getting Started

##### Run server locally
Clone project & setup env vars
```bash
git clone git@gitlab.com:hackaton-int20h/testtaskfront-2020.git
cd hackaton-int20h
npm i
cp .env.example .env.local
nano .env
npm run start
``` 
### Prerequisites

Check if the following programs & frameworks is installed on your pc
* [Node>=10](https://nodejs.org/download/) 

### And coding style tests

```bash
npm run lint
npm run prettier
```

## Deployment

By hands

## Built With

* React
* Ant design UI

## Authors

* **Danylo Kazymyrov** - *Frontend* - [danylkaaa](https://gitlab.com/danylkaaa)
* **Lelikov Kazymyrov** - *Backend* - [kostialelikov](https://gitlab.com/kostialelikov)
* **Anna Korunska** - *Frontend* - [akorunska](https://gitlab.com/akorunska)
* **Ruckhailo Pasha** - *Frontend* - [IceBroForever](https://gitlab.com/IceBroForever)

## License

This project is licensed under the MIT License
