export const createReducers = (reducers) => (state, action) => {
    let wrappedAction = action;
    if (typeof action === 'string') {
        wrappedAction = { type: action };
    }
    if (!reducers[wrappedAction.type]) {
        throw new Error(`Cannot find reducer for ${wrappedAction.type}`);
    }
    return reducers[wrappedAction.type](state, wrappedAction);
};

export const wrapWithCors = (url) =>
    `https://cors-anywhere.herokuapp.com/${url}`;
