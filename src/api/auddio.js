import axios from 'axios';
import * as c from './constants';

export const predictSongByText = async (text) => {
    const response = await axios({
        url: c.AUDDIO_URL_FIND_BY_LYRICS,
        method: 'post',
        params: {
            return: c.AUDDIO_RETURNING_FIELDS,
            q: text,
            api_token: c.AUDDIO_ACCESS_TOKEN,
        },
    });
    return response.data;
};
