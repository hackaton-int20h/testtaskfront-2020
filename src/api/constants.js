import { wrapWithCors } from '../utils';

export const AUDDIO_ACCESS_TOKEN = process.env.REACT_APP_AUDDIO_ACCESS_TOKEN;
export const AUDDIO_RETURNING_FIELDS = 'timecode,apple_music,deezer,spotify';
export const AUDDIO_URL_FIND_BY_LYRICS = wrapWithCors(
    'https://api.audd.io/findLyrics',
);
export const DEEZER_API_URL = wrapWithCors('https://api.deezer.com/search');
export const OUR_API_URL =
    'https://bu9xf0wm22.execute-api.us-east-1.amazonaws.com';
