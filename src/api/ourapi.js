import { OUR_API_URL } from './constants';

export const predictBySound = async (sound) => {
    const formData = new FormData();
    formData.append('sound', sound);

    const response = await fetch(`${OUR_API_URL}/dev/sound`, {
        method: 'post',
        mode: 'cors',
        body: formData,
    });

    return response.json();
};
