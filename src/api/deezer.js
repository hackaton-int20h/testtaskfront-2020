import axios from 'axios';
import * as c from './constants';

export const findDeezerPreview = (artist, track) => {
    return axios({
        url: c.DEEZER_API_URL,
        method: 'get',
        params: {
            q: track,
            artist,
        },
    });
};
