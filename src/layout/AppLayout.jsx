import React from 'react';
import PropTypes from 'prop-types';
import { Col, Layout, Row } from 'antd';
import AppHeader from './Header';

const AppLayout = ({ children }) => {
    return (
        <>
            <Layout style={{ height: '100vh', width: '100vw' }}>
                <AppHeader />
                <Layout.Content>
                    <div style={{ height: 'calc(100vh - 55px)' }}>
                        <Row justify="center">
                            <Col
                                xs={24}
                                md={{ span: 16, offset: 4 }}
                                xl={{ span: 12, offset: 6 }}
                            >
                                {children}
                            </Col>
                        </Row>
                    </div>
                </Layout.Content>
                <Layout.Footer style={{ textAlign: 'center' }}>
                    by 4 Pacana team
                </Layout.Footer>
            </Layout>
        </>
    );
};

AppLayout.propTypes = {
    children: PropTypes.element.isRequired,
};

export default AppLayout;
