import * as React from 'react';
import * as PropTypes from 'prop-types';

import { initialState, reducers } from '../reducers/store.reducer';

export const StoreContext = React.createContext({});

export const StoreProvider = ({ children }) => {
    const [state, dispatch] = React.useReducer(reducers, initialState);

    return (
        <StoreContext.Provider value={{ state, dispatch }}>
            {children}
        </StoreContext.Provider>
    );
};

StoreProvider.propTypes = {
    children: PropTypes.element.isRequired,
};
