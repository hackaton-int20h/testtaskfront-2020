import React from 'react';
import AppLayout from './layout/AppLayout';
import { HomePage } from './pages/Home';
import './App.css';
import { StoreProvider } from './store/Store';

export const App = () => {
    return (
        <>
            <StoreProvider>
                <AppLayout>
                    <HomePage />
                </AppLayout>
            </StoreProvider>
        </>
    );
};
