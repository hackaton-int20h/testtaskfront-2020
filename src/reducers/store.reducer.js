import { createReducers } from '../utils';

export const initialState = {
    isLoading: false,
    song: null,
    songCdn: null,
    error: null,
    recAudio: null,
};

export const actions = {
    API_CALL_START: 'API_CALL_START',
    API_CALL_FAIL: 'API_CALL_FAIL',
    API_CALL_FINISH: 'API_CALL_FINISH',
    FIND_SONG: 'FIND_SONG',
    FIND_SONG_CDN: 'FIND_SONG_CDN',
    SET_REC_AUDIO: 'SET_REC_AUDIO',
    CLEAR_ERROR: 'CLEAR_ERROR',
};

const startApiCall = (state) => ({
    ...state,
    isLoading: true,
    songCdn: null,
    song: null,
});

const setRecAudio = (state, action) => ({
    ...state,
    recAudio: action.data,
});

const clearError = (state) => ({
    ...state,
    error: null,
});

const failApiCall = (state, action) => ({
    ...state,
    isLoading: false,
    error: action.error,
    songCdn: null,
    song: null,
});

const finishApiCall = (state) => ({
    ...state,
    isLoading: false,
    error: null,
    songCdn: null,
    song: null,
});

const findSong = (state, action) => ({
    ...state,
    isLoading: false,
    song: action.data,
    songCdn: null,
});

const findSongCdn = (state, action) => ({
    ...state,
    isLoading: false,
    songCdn: action.data,
});

export const reducersDeclaration = {
    [actions.API_CALL_START]: startApiCall,
    [actions.API_CALL_FAIL]: failApiCall,
    [actions.API_CALL_FINISH]: finishApiCall,
    [actions.CLEAR_ERROR]: clearError,
    [actions.FIND_SONG]: findSong,
    [actions.FIND_SONG_CDN]: findSongCdn,
    [actions.SET_REC_AUDIO]: setRecAudio,
};

export const reducers = createReducers(reducersDeclaration);
