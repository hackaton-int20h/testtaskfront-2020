import * as React from 'react';
import { findDeezerPreview } from '../api';
import { actions } from '../reducers/store.reducer';
import { useStoreContext } from './useStoreContext';

export const useLoadDeezerPreview = () => {
    const {
        state: { song, songCdn },
        dispatch,
    } = useStoreContext();

    React.useEffect(() => {
        const makeApiCall = async () => {
            if (!song) return;
            try {
                const songInfo = await findDeezerPreview(
                    song.artist,
                    song.title,
                );
                if (!songInfo.data.data || !songInfo.data.data[0]) {
                    throw new Error('Не удалось найти песню на Deezer(((');
                }
                dispatch({
                    type: actions.FIND_SONG_CDN,
                    data: songInfo.data.data[0].preview,
                });
            } catch (e) {
                dispatch({ type: actions.API_CALL_FAIL, error: e.message });
            }
        };
        makeApiCall(song);
    }, [song]);

    return {
        songCdn,
    };
};
