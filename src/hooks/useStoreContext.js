import * as React from 'react';
import { StoreContext } from '../store/Store';

export const useStoreContext = () => {
    const context = React.useContext(StoreContext);
    if (context === undefined) {
        throw new Error('useStoreState must be used within a StoreProvider');
    }
    return context;
};
