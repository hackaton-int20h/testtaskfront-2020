import * as React from 'react';
import { predictSongByText } from '../api';
import { actions } from '../reducers/store.reducer';
import { useStoreContext } from './useStoreContext';

export const usePredictSongByText = () => {
    const [text, setText] = React.useState("I don't wanna fall, fall away");
    const { dispatch, state } = useStoreContext();

    const makePredictSongCall = React.useCallback(async (songText) => {
        if (!songText) {
            return;
        }
        dispatch(actions.API_CALL_START);
        try {
            const { status, result } = await predictSongByText(songText);
            if (status !== 'success' || !result.length) {
                throw new Error(
                    'Нам не удалось найти трек((( Го поищем что-то другое?',
                );
            }
            const songInfo = result[0];
            songInfo.media = JSON.parse(songInfo.media);
            dispatch({ type: actions.API_CALL_FINISH });
            dispatch({ type: actions.FIND_SONG, data: songInfo });
        } catch (e) {
            dispatch({ type: actions.API_CALL_FAIL, error: e.message });
        }
    }, []);

    return {
        text,
        isLoading: state.isLoading,
        makePredictSongCall: () => makePredictSongCall(text),
        onTextChange: (e) => {
            setText(e.currentTarget.value);
        },
    };
};
