import * as React from 'react';
import { useStoreContext } from './useStoreContext';
import { actions } from '../reducers/store.reducer';

const recordAudio = () => {
    return new Promise((resolve) => {
        navigator.mediaDevices.getUserMedia({ audio: true }).then((stream) => {
            const mediaRecorder = new MediaRecorder(stream);
            const audioChunks = [];

            mediaRecorder.addEventListener('dataavailable', (event) => {
                audioChunks.push(event.data);
            });

            const start = () => {
                mediaRecorder.start();
            };

            const stop = () => {
                return new Promise((resolve2) => {
                    mediaRecorder.addEventListener('stop', async () => {
                        const audioBlob = new Blob(audioChunks);
                        const audioUrl = URL.createObjectURL(audioBlob);
                        const audio = new Audio(audioUrl);
                        const play = () => {
                            audio.play();
                        };

                        resolve2({
                            audioBlob,
                            audioUrl,
                            play,
                        });
                    });

                    mediaRecorder.stop();
                });
            };

            resolve({ start, stop });
        });
    });
};

export const useMicRecord = () => {
    const { dispatch } = useStoreContext();
    const [running, setRunning] = React.useState(false);
    const [recorder, setRecorder] = React.useState(null);
    const [audio, setAudio] = React.useState(null);

    React.useEffect(() => {
        (async () => {
            if (!running && recorder) {
                const capturedAudio = await recorder.stop();
                setAudio(capturedAudio);
                dispatch({
                    type: actions.SET_REC_AUDIO,
                    data: capturedAudio.audioBlob,
                });
            } else if (!running) {
                setAudio(null);
            } else {
                const newRecorder = await recordAudio();
                newRecorder.start();
                setRecorder(newRecorder);
            }
        })();
    }, [running]);

    return { audio, running, setRunning, setAudio };
};
