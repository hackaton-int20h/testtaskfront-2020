import * as React from 'react';
import { predictBySound } from '../api';
import { actions } from '../reducers/store.reducer';
import { useStoreContext } from './useStoreContext';

const makeMediaProvider = (song, provider) => {
    if (song[provider]) {
        song.media.push({
            provider,
            url: song[provider],
        });
    }
};

export const usePredictSongByAudio = () => {
    const { dispatch, state } = useStoreContext();
    const { recAudio } = state;

    const makePredictSongCall = React.useCallback(async (audio) => {
        if (!audio) {
            return;
        }
        dispatch(actions.API_CALL_START);
        try {
            const songInfo = await predictBySound(audio);

            if (!songInfo) {
                throw new Error(
                    'Нам не удалось распознать трек((( Может ты поешь лучше автора?',
                );
            }
            songInfo.media = [];
            [
                'apple_music',
                'spotify',
                'deezer',
                'youtube',
            ].forEach((provider) => makeMediaProvider(songInfo, provider));
            dispatch({ type: actions.API_CALL_FINISH });
            dispatch({ type: actions.FIND_SONG, data: songInfo });
        } catch (e) {
            dispatch({ type: actions.API_CALL_FAIL, error: e.message });
        }
    }, []);

    return {
        isLoading: state.isLoading,
        makePredictSongCall: () => makePredictSongCall(recAudio),
    };
};
