import React, { useEffect } from 'react';
import { message, PageHeader } from 'antd';
import { SearchCarousel } from '../components/SearchCarousel';
import { ApiCallsLoader } from '../components/ApiCallsLoader';
import { useStoreContext } from '../hooks/useStoreContext';
import { SongInfo } from '../components/SongInfo';
import { Quiz } from '../components/Quiz';
import { AppSteps } from '../components/AppSteps';
import { actions } from '../reducers/store.reducer';

export const HomePage = () => {
    const { state, dispatch } = useStoreContext();
    useEffect(() => {
        if (state.error) {
            message.error(state.error, () => dispatch(actions.CLEAR_ERROR));
        }
    }, [state.error]);
    return (
        <>
            <PageHeader title="Акинатор для меломанов" />
            <AppSteps />
            <SearchCarousel />
            <ApiCallsLoader />
            <SongInfo song={state.song} />
            <Quiz />
        </>
    );
};
