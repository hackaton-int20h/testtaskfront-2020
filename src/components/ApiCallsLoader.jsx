import React from 'react';
import { Skeleton } from 'antd';
import { usePredictSongByText } from '../hooks/usePredictSongByText';

export const ApiCallsLoader = () => {
    const { isLoading } = usePredictSongByText();
    if (isLoading) {
        return <Skeleton active />;
    }
    return null;
};
