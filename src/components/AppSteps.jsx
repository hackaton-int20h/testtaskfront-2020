import React from 'react';
import { Icon, Steps } from 'antd';
import { useStoreContext } from '../hooks/useStoreContext';

export const AppSteps = () => {
    const [current, setCurrent] = React.useState(1);
    const { state } = useStoreContext();

    React.useEffect(() => {
        if (!state.isLoading && !state.song) {
            setCurrent(0);
        } else if (state.isLoading && !state.song) {
            setCurrent(1);
        } else if (state.song && !state.songCdn) {
            setCurrent(2);
        } else {
            setCurrent(3);
        }
    }, [state.isLoading, state.song, state.songCdn]);

    return (
        <Steps current={current} status={state.error ? 'error' : null}>
            <Steps.Step
                icon={<Icon type="question-circle" />}
                title="Начни вводить"
                description=""
            />
            <Steps.Step
                icon={
                    <Icon
                        type={current === 1 ? 'loading' : 'customer-service'}
                    />
                }
                title="Ищем песню"
                subTitle=""
                description={
                    current === 1
                        ? 'Лучшие умы из audd.io пытаются угадать песню'
                        : null
                }
            />
            <Steps.Step
                icon={<Icon type={current === 2 ? 'loading' : 'scissor'} />}
                title="Ищем образец"
                description={
                    current === 2
                        ? 'Обрезаем трек, чтобы нас не поймали за пиратство'
                        : null
                }
            />
            <Steps.Step
                icon={<Icon type="sound" />}
                title="Результат"
                description={current === 3 ? 'Наслаждайся' : null}
            />
        </Steps>
    );
};
