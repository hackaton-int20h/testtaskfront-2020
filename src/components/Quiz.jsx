import React, { useState } from 'react';
import { Button, Icon, Result } from 'antd';
import { useStoreContext } from '../hooks/useStoreContext';

export const Quiz = () => {
    const { state } = useStoreContext();
    const [status, setStatus] = useState('info');
    const [success, setSuccess] = useState(null);

    React.useEffect(() => {
        setSuccess(null);
    }, [state.song]);

    if (!state.song) {
        return null;
    }

    return (
        <Result
            status={status}
            icon={
                success !== null ? (
                    <Icon type={success ? 'smile' : 'frown'} theme="twoTone" />
                ) : null
            }
            title="Мы угадали твою песню?"
            extra={
                success === null
                    ? [
                          <Button
                              type="primary"
                              onClick={() => setSuccess(true)}
                              onMouseEnter={() => setStatus('success')}
                              onMouseLeave={() => setStatus('info')}
                          >
                              Угадали
                          </Button>,
                          <Button
                              type="danger"
                              onClick={() => setStatus('error')}
                              onMouseEnter={() => setStatus('warning')}
                              onMouseLeave={() => setStatus('info')}
                          >
                              Нет
                          </Button>,
                      ]
                    : null
            }
        />
    );
};
