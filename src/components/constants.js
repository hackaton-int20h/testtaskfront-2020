export const SOUND_DERVICES_LINKS = {
    apple_music: {
        displayName: 'Apple Music',
        logoLink:
            'https://www.pngitem.com/pimgs/m/502-5029471_vector-apple-music-logo-hd-png-download.png',
    },
    spotify: {
        displayName: 'Spotify',
        fieldName: 'spotify',
        logoLink:
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Spotify_logo_without_text.svg/1024px-Spotify_logo_without_text.svg.png',
    },
    youtube: {
        displayName: 'YouTube',
        fieldName: 'youtube',
        logoLink:
            'https://cdn3.iconfinder.com/data/icons/popular-services-brands/512/youtube-512.png',
    },
    soundcloud: {
        displayName: 'Soundcloud',
        fieldName: 'soundcloud',
        logoLink:
            'https://c7.hotpng.com/preview/946/592/881/soundcloud-computer-icons-logo-youtube-sound.jpg',
    },
};
