import React from 'react';
import * as PropTypes from 'prop-types';
import { Card, Typography, Divider, List } from 'antd';
import styled from 'styled-components';
import { SongServiceItem } from './SongServiceItem';
import { Player } from './Player';

const { Title } = Typography;

const SongData = styled.div`
    padding-top: 0.5em;
    line-height: 2rem;
    h2,
    h4 {
        color: white;
    }
    a {
        color: white;
    }
`;

export const SongInfo = ({ song }) => {
    if (!song) {
        return null;
    }

    return (
        <>
            <Card style={{ marginTop: 20, background: '#002329' }}>
                <SongData>
                    <Divider orientation="left">
                        <Title
                            level={2}
                        >{`"${song.title}" by ${song.artist}`}</Title>
                    </Divider>
                    <Player />
                    <Divider orientation="left">
                        <Title level={4}>Available at music services:</Title>
                    </Divider>
                    <List
                        grid={{
                            gutter: 16,
                            xs: 1,
                            sm: 2,
                            md: 4,
                            lg: 4,
                            xl: 4,
                            xxl: 4,
                        }}
                        dataSource={song.media}
                        renderItem={(item) => {
                            return <SongServiceItem mediaProvider={item} />;
                        }}
                    />
                </SongData>
            </Card>
        </>
    );
};

SongInfo.propTypes = {
    song: PropTypes.object,
};

SongInfo.defaultProps = {
    song: null,
};
