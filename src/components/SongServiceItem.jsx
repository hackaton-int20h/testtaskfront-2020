import * as React from 'react';
import * as PropTypes from 'prop-types';
import { Avatar, List } from 'antd';
import { SOUND_DERVICES_LINKS } from './constants';

export const SongServiceItem = ({ mediaProvider }) => {
    const provider = SOUND_DERVICES_LINKS[mediaProvider.provider];
    if (!provider) return null;

    return (
        <List.Item>
            <List.Item.Meta
                avatar={<Avatar src={provider.logoLink} />}
                title={<a href={mediaProvider.url}>{provider.displayName}</a>}
            />
        </List.Item>
    );
};

SongServiceItem.propTypes = {
    mediaProvider: PropTypes.object.isRequired,
};
