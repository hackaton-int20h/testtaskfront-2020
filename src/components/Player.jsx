import React from 'react';
import ReactAudioPlayer from 'react-audio-player';
import { Skeleton } from 'antd';
import { useLoadDeezerPreview } from '../hooks/useLoadDeezerPreview';

export const Player = () => {
    const { songCdn } = useLoadDeezerPreview();

    if (!songCdn) {
        return <Skeleton loading />;
    }

    return (
        <ReactAudioPlayer
            style={{ width: '100%' }}
            src={songCdn}
            autoPlay
            controls
        />
    );
};
