import React from 'react';
import { Button, Statistic, Row, Col, message } from 'antd';
import Card from 'antd/es/card';
import { useMicRecord } from '../hooks/useMicRecord';
import { usePredictSongByAudio } from '../hooks/usePredictSongByAudio';

const AudioSongSearchForm = () => {
    const { audio, setRunning, running, setAudio } = useMicRecord();
    const [timeStart, setTimeStart] = React.useState(null);
    const { makePredictSongCall } = usePredictSongByAudio();
    const clear = () => {
        setRunning(false);
        setAudio(null);
        setTimeStart(null);
    };
    const start = () => {
        setRunning(!running);
        setTimeStart(Date.now());
    };

    const finish = () => {
        setRunning(false);
        if (Date.now() - timeStart < 5 * 10 ** 3) {
            message.warning(
                'О нет, твой войс слишком короткий (<5 сек), может не найти песню',
            );
            // clear();
        }
        setTimeStart(null);
    };

    React.useEffect(
        () => () => {
            clear();
        },
        [],
    );

    return (
        <>
            <Card title="Запиши войс, а мы угадаем что ты напел">
                <Row justify="center">
                    {!running ? (
                        <Button
                            icon="play-circle"
                            onClick={start}
                            type="primary"
                        >
                            {' '}
                            {!audio ? 'Начать запись' : 'Перезаписть'}
                        </Button>
                    ) : (
                        <Button icon="pause" onClick={finish} type="">
                            {' '}
                            Закончить запись
                        </Button>
                    )}
                </Row>
                <Row justify="center">
                    {running && (
                        <Col span={4}>
                            <Statistic.Countdown
                                format="ss:SSS"
                                title="Осталось времени"
                                value={timeStart + 10 ** 4}
                                onFinish={finish}
                            />
                        </Col>
                    )}
                </Row>
            </Card>
            <Row>
                {!!audio && (
                    <Button
                        type="primary"
                        htmlType="submit"
                        icon="search"
                        onClick={makePredictSongCall}
                    >
                        Начать поиск
                    </Button>
                )}
            </Row>
        </>
    );
};

export default AudioSongSearchForm;
