import React from 'react';
import { Input, Button, Spin } from 'antd';
import { usePredictSongByText } from '../hooks/usePredictSongByText';

const { TextArea } = Input;

const TextSongSearchForm = () => {
    const {
        makePredictSongCall,
        onTextChange,
        isLoading,
        text,
    } = usePredictSongByText();

    return (
        <>
            <Spin spinning={isLoading}>
                <TextArea
                    rows={10}
                    onPressEnter={makePredictSongCall}
                    name="lyrics"
                    onChange={onTextChange}
                    value={text}
                />
            </Spin>
            <Button type="primary" onClick={makePredictSongCall}>
                Что я несу?
            </Button>
        </>
    );
};

export default TextSongSearchForm;
