import React from 'react';
import { Card, Carousel } from 'antd';
import TextSongSearchForm from './TextSongSearch';
import AudioSongSearchForm from './AudioSongSearch';

export const SearchCarousel = () => {
    return (
        <>
            <Card style={{ marginTop: 20, background: '#002329' }}>
                <Carousel>
                    <TextSongSearchForm />
                    <AudioSongSearchForm />
                </Carousel>
            </Card>
        </>
    );
};
