const path = require('path');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
	entry: {
		app: './src/index.js',
	},
	mode: 'development',
	devtool: 'inline-source-map',
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel-loader',
				options: { presets: ['@babel/env'] },
			},
			{
				test: /\.css$/,
				use: [
					'style-loader',
					{
						loader: 'css-loader',
						options: {
							modules: true,
						},
					},
				],
			},
			{
				test: /\.(png|svg|jpg|gif)$/,
				use: ['file-loader'],
			},
		],
	},
	resolve: { extensions: ['*', '.js', '.jsx'] },
	output: {
		filename: '[name].bundle.js',
		path: path.resolve(__dirname, 'dist'),
		publicPath: '/dist/',
	},
	devServer: {
		contentBase: path.join(__dirname, 'public/'),
		hot: true,
		port: 9000,
		publicPath: 'http://localhost:3000/dist/',
		hotOnly: true,
		compress: true,
		watchContentBase: true,
		progress: true,
	},
	plugins: [new CleanWebpackPlugin(), new webpack.HotModuleReplacementPlugin()],
};
